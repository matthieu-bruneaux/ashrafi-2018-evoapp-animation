# Description

This repository contains the data and code to generate an animation to
illustrate the paper by Ashrafi et al. 2018:

- Ashrafi, Roghaieh, Matthieu Bruneaux, Lotta-Riina Sundberg, Katja Pulkkinen,
Janne Valkonen, and Tarmo Ketola. **Broad Thermal Tolerance Is Negatively
Correlated with Virulence in an Opportunistic Bacterial Pathogen.** Evolutionary
Applications 11, no. 9 (2018): 1700–1714. https://doi.org/10.1111/eva.12673.

# Animation

The animated graph of the evolution of water temperature in a fish farm from
Central Finland over more than 40 years is available
[here](https://matthieu-bruneaux.gitlab.io/ashrafi-2018-evoapp-animation/animation-central-finland-fish-farm.mp4).
